import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

import './App.css';
import store from './redux/store';
import { Provider } from 'react-redux';

/* importing style */
import styles from './assets/css/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import ProfileEdit from './features/profile/edit/index';

const App = () => {
  return (
    <Provider store={store}>
        <Router basename={process.env.PUBLIC_URL}>
          <Route exact path='/' component={ProfileEdit} />
        </Router>
      </Provider>
  );
};

export default App;
