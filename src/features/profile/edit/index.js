import React, {Component} from 'react'
import { Button, Card, Col, Form, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Select from 'react-select';

class Index extends Component {
    state = {
        formMealData: [{ meal: '', drink: '' }],
      };

    handleChange(i, e, name) {
        let formMealData = this.state.formMealData;
        
        if(name === 'meal'){
            formMealData[i]['meal'] = e.value;

        } else if(name === 'drink'){
            formMealData[i]['drink'] = e.value;
        }
        
        this.setState({ formMealData });
    }
    
    addMoreMeal() {
        this.setState(({
            formMealData: [...this.state.formMealData, { meal: "", drink: "" }]
        }))
    }

    removeMeals(i) {
        let formMealData = this.state.formMealData;
        formMealData.splice(i, 1);
        this.setState({ formMealData });
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state.formMealData)
    }

    handleDelete(e) {
        e.preventDefault();
        console.log('deleting', e)
    }

    render () {

        return (
            <>
            <Form onSubmit = {(e)=>this.handleSubmit(e)}>
            <div className='page'>
            <div className='header'>Edit Profile</div>
            <div className='main'>
                <Card>
                <Card.Title>Personal Details</Card.Title>
                <Card.Body>
                    <Row>
                        <Col>
                        <Form.Group className="mb-3" controlId="formFirstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control type="text" />
                        </Form.Group>
                        </Col>
                        <Col>
                        <Form.Group className="mb-3" controlId="formLastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control type="text" />
                        </Form.Group>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                        <Form.Group className="mb-3" controlId="formMobile">
                            <Form.Label>Mobile</Form.Label>
                            <Form.Control type="text" />
                        </Form.Group>
                        </Col>
                        <Col>
                        <Form.Group className="mb-3" controlId="formEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" />
                        </Form.Group>
                        </Col>
                    </Row>

                </Card.Body>
                </Card>

                <Card>
                <Card.Title>Meal Preferences</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">Please add all meal preferences.</Card.Subtitle>
                <Card.Body>
                {this.state.formMealData.map((element, index) => (
                    <Row>
                    <Col md={3}>
                        <Form.Group className="mb-3"  key={index}>
                        <Form.Label>Meal {index+1}</Form.Label>
                        <Select name='meal' defaultValue={[{ value: 'sausage', label: 'Sausage Egg Scramble Bowl' }]}
                            options={[
                            { value: 'sausage', label: 'Sausage Egg Scramble Bowl' },
                            { value: 'rice', label: 'Fried Rice' },
                        ]}
                        onChange={e=>this.handleChange(index, e, 'meal')} />

                        </Form.Group>
                    </Col>
                    <Col md={3}>
                        <Form.Group className="mb-3">
                        <Form.Label>Drink</Form.Label>
                        
                        <Select name='drink' defaultValue={[{ value: 'coffee', label: 'Coffee' }]} 
                            options={[
                            { value: 'coffee', label: 'Coffee' },
                            { value: 'cranberry-juice', label: 'Cranberry Juice' },
                        ]}
                        onChange={e=>this.handleChange(index, e, 'drink')} />

                        </Form.Group>
                    </Col>
                    <Col md={2}>
                    { 
                        index > 0 && <Link onClick={() => this.removeMeals(index)} className='remove-link'><i class="fa fa-trash" aria-hidden="true"></i></Link>
                    }
                    </Col>

                </Row>
                ))}
                
                <Row>
                    <Col>
                    <Button variant="primary" type="button" onClick={() => this.addMoreMeal()}>
                    + Add Meal
                    </Button>
                    </Col>
                </Row> 

                </Card.Body>
                </Card>
            </div>
            <div className='footer'>
                <Button variant="primary" className='btn-black' type="submit" onClick={(e) => this.handleSubmit(e)}>
                    Save
                </Button>
                
            </div>
            </div>
            </Form>
            </>
        )
    }
}

export default Index;